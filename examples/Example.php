<?php
/**
 * Class describing how to use provided Id4Me mechanisms supported by current library
 */


use GuzzleHttp\Client as GuzzleClient;
use Id4me\RP\Service;

include '../vendor/autoload.php';

class Example
{
    /**
     * @var \Id4me\RP\Service
     */
    private $id4Me = null;

    public function __construct()
    {
        $this->httpClient = new GuzzleClient();
        $this->id4Me = new Service();
    }

    /**
     * Main function running Id4Me mechanisms supported by current library
     */
    public function run()
    {
        $identifier = 'idtemp2.id4me.family';
        echo PHP_EOL;
        echo '***********************************Discovery***************************************';
        echo PHP_EOL;
        $authorityName = $this->id4Me->discover($identifier);
        var_dump($authorityName);
        echo PHP_EOL;
        echo PHP_EOL;
        echo '***********************************Registration***************************************';
        echo PHP_EOL;
        $openIdConfig = $this->id4Me->getOpenIdConfig($authorityName);
        var_dump($openIdConfig);
        echo PHP_EOL;
        $client = $this->id4Me->register(
            $openIdConfig,
            $identifier,
            sprintf('http://www.rezepte-elster.de/id4me.php', $identifier)
        );
        var_dump($client);
        echo PHP_EOL;
        echo PHP_EOL;
        echo '***********************************Authenticate***************************************';
        echo PHP_EOL;
        echo "Do following steps:\n";
        echo "1.Please click on login link below\n";
        echo "2.Login with password '123456'\n";
        echo "3.Copy and Paste 'code' value from corresponding url query parameter into code input prompt field below'\n";
        $authorizationUrl = $this->id4Me->getAuthorizationUrl(
            $openIdConfig, $client->getClientId(), $identifier, $client->getActiveRedirectUri(), 'idtemp2.id4me.family'
        );
        var_dump($authorizationUrl);
        echo PHP_EOL;
        echo PHP_EOL;
        $accessTokens = $this->id4Me->getAccessTokens(
            $openIdConfig,
            readline('code:'),
            sprintf('http://www.rezepte-elster.de/id4me.php', $identifier),
            $client->getClientId(),
            $client->getClientSecret()
        );

        var_dump($accessTokens);
        echo PHP_EOL;
        echo PHP_EOL;
    }


    /**
     * Set current http client to another value
     *
     * @param Client $httpClient
     */
    public function setHttpClient(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }
}

$action = new Example();
$action->run();
