<?php

use Id4me\RP\Helper\OpenIdConfigHelper;
use Id4me\RP\Model\Client;
use Id4me\RP\Model\OpenIdConfig;
use Id4me\RP\Registration;

class RegistrationTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test for Registration::registerClient()
     */
    public function testRegisterClient()
    {
        $registration = new Registration();

        $client = $this->createClient();

        $retrievedClient = $registration->register(
            $this->getOpenIdConfig(),
            'rezepte-elster.de',
            'http://rezepte-elster.de'
        );

        $client->setClientSecret($retrievedClient->getClientSecret());
        $client->setClientId($retrievedClient->getClientId());

        $this->assertEquals($client, $retrievedClient);
    }

    /**
     * Creates a Client Instance
     *
     * @return Client
     */
    private function createClient()
    {
        $client = new Client('https://id.denic.de');

        $client->setClientName('rezepte-elster.de');
        $client->setClientId('clientId');
        $client->setClientSecret('clientSecret');
        $client->setClientExpirationTime(0);
        $client->setActiveRedirectUri('http://rezepte-elster.de');
        $client->setRedirectUris(['http://rezepte-elster.de']);

        return $client;
    }

    /**
     * Retrieves an instance of OpenIdConfig from fetched json config data
     *
     * @return OpenIdConfig
     */
    private function getOpenIdConfig()
    {
        return OpenIdConfigHelper::instance()->createFromJson(
            file_get_contents(
                sprintf('%s/mocks/openIdConfigJson.json', __DIR__)
            )
        );
    }
}
