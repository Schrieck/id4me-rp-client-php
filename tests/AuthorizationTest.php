<?php

use Id4me\RP\Authorization;
use Id4me\RP\Model\OpenIdConfig;

class AuthorizationTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test for Authorization::getAuthorizationUrl()
     *
     * @throws Exception
     */
    public function testGetAuthorizationUrl()
    {
        $authEndpoint = "http://local.host/login";

        $mock = $this->createMock(OpenIdConfig::class);
        $mock->method("getAuthorizationEndpoint")->willReturn($authEndpoint);

        $authorization = new Authorization();

        $clientId    = "clientId";
        $identifier  = "example.org";
        $redirectUri = "http://example.org";
        $state       = "test";

        $expectedUrl = $authEndpoint
                       . "?client_id=" . $clientId
                       . "&login_hint=" . $identifier
                       . "&redirect_uri=" . urlencode($redirectUri)
                       . "&scope=openid"
                       . "&prompt=login"
                       . "&response_type=code"
                       . "&state=" . $state;

        $this->assertEquals(
            $expectedUrl,
            $authorization->getAuthorizationUrl(
                $mock->getAuthorizationEndpoint(),
                $clientId,
                $identifier,
                $redirectUri,
                $state
            )
        );
    }
}
