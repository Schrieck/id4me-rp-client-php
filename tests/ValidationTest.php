<?php

use Id4me\RP\Exception\InvalidAuthorityIssuerException;
use Id4me\RP\Exception\InvalidIDTokenException;
use Id4me\RP\Helper\JWSHelper;
use Id4me\RP\Validation;

class ValidationTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test for Validation::validateIDToken() failing by Validation::validateIdTokenSignature()
     *
     * @param boolean $isValidSignature
     * @param boolean $hasUnExpectedException
     * @param string $exceptionMessage
     *
     * @throws InvalidAuthorityIssuerException
     * @throws InvalidIDTokenException
     *
     * @dataProvider getIDTokenDataForSignatureValidation()
     */
    public function testValidateIDTokenWithInValidSignature(
        bool $isValidSignature,
        bool $hasUnExpectedException,
        string $exceptionMessage
    ) {
        if (! $isValidSignature) {
            $this->expectException(InvalidIDTokenException::class);
            $this->expectExceptionMessage($exceptionMessage);
        }

        $validation = new Validation();
        $validation->setJWSHelper($this->getJWSHelper($isValidSignature, $hasUnExpectedException));

        $validation->validateIDToken('issuer', 'clientId', 'IdToken', 'RSA Key Data');
    }

    /**
     * Test for Validation::validateIDToken() failing by Validation::validateISS()
     *
     * @throws InvalidAuthorityIssuerException
     * @throws InvalidIDTokenException
     */
    public function testValidateIDTokenWithInvalidIssuerData()
    {
        $iss    = 'https://iss.example.com';
        $issuer = 'https://issuer.example.com';

        $this->expectException(InvalidAuthorityIssuerException::class);
        $this->expectExceptionMessage(Validation::INVALID_ID_TOKEN_ISSUER_EXCEPTION);

        $validation = new Validation();
        $validation->setJWSHelper(
            $this->getJWSHelper(
                true,
                false,
                json_encode(['iss' => $iss])
            )
        );

        $validation->validateIDToken($issuer, 'clientId', 'IdToken', 'RSA Key Data');
    }

    /**
     * Test for Validation::validateIDToken() failing by Validation::validateAudience()
     *
     * @param array|string $audience
     * @param       string $clientId
     * @param       mixed $idTokenValue
     *
     * @dataProvider getIdTokenDataWithInvalidAudience()
     *
     * @throws InvalidAuthorityIssuerException
     * @throws InvalidIDTokenException
     */
    public function testValidateIDTokenWithInvalidAudience($audience, $clientId, $idTokenValue)
    {
        $issuer  = 'issuer';
        $idToken = (empty($idTokenValue)) ? json_encode(['iss' => $issuer, 'aud' => $audience]) : $idTokenValue;

        $this->expectException(InvalidIDTokenException::class);
        $this->expectExceptionMessage(Validation::INVALID_ID_TOKEN_AUDIENCE_EXCEPTION);

        $validation = new Validation();
        $validation->setJWSHelper(
            $this->getJWSHelper(
                true,
                false,
                $idToken
            )
        );

        $validation->validateIDToken($issuer, $clientId, 'IdToken', 'RSA Key Data');
    }

    /**
     * Test for Validation::validateIDToken() failing by Validation::validateExpirationTime()
     *
     * @throws InvalidAuthorityIssuerException
     * @throws InvalidIDTokenException
     */
    public function testValidateIDTokenWithInvalidExpirationTime()
    {
        $issuer         = 'issuer';
        $audience       = 'clientId';
        $clientId       = 'clientId';
        $expirationTime = mktime(10, 30, 30, 1, 1, 1950);

        $this->expectException(InvalidIDTokenException::class);
        $this->expectExceptionMessage(Validation::INVALID_ID_TOKEN_EXPIRATION_TIME_EXCEPTION);

        $validation = new Validation();
        $validation->setJWSHelper(
            $this->getJWSHelper(
                true,
                false,
                json_encode(['iss' => $issuer, 'aud' => $audience, 'exp' => $expirationTime])
            )
        );

        $validation->validateIDToken($issuer, $clientId, 'IdToken', 'RSA Key Data');
    }

    /**
     * Test for Validation::validateIDToken()
     *
     * @param string $issuer
     * @param string $clientId
     * @param string $rsaKeyData
     * @param string $idTokenValue
     *
     * In this case ID Token passes all validation steps 1. to 3. and 9.
     * of 4.5.3. ID Token Validation Specifications of ID4Me and no exception shall be thrown
     *
     * @dataProvider getIdTokenDataInHappyPath()
     *
     * @throws Exception
     */
    public function testValidateIDTokenInHappyPath(
        string $issuer,
        string $clientId,
        string $rsaKeyData,
        string $idTokenValue
    ) {
        $validation = new Validation();
        $validation->setJWSHelper(
            $this->getJWSHelper(
                true,
                false,
                $idTokenValue
            )
        );

        $validation->validateIDToken($issuer, $clientId, $idTokenValue, $rsaKeyData);

        $this->addToAssertionCount(1);
    }

    /**
     * Retrieves data to test validateIDToken
     *
     * @return array
     */
    public function getIDTokenDataForSignatureValidation()
    {
        return [
            [false, true, 'Invalid RSA Key provided'],
            [false, false, 'Invalid ID Token Signature: IdToken']
        ];
    }

    /**
     * Retrieves data to test ID Token Data with invalid audience values
     *
     * @return array
     */
    public function getIdTokenDataWithInvalidAudience()
    {
        return [
            [null, 'clientId', null],
            [false, 'clientId', null],
            ['', 'clientId', null],
            ['IDTokenAudience', 'clientId', null],
            [[], 'clientId', null],
            [['AnyUnknownClientId', 'garbage', 'foo', 'bar'], 'clientId', null],
            [
                ['AnyUnknownClientId', 'garbage', 'foo', 'bar'],
                'clientId',
                json_encode(
                    [
                        'iss' => 'issuer',
                        'azp' => 'unknownClientId',
                        'aud' => ['clientId', 'garbage', 'foo', 'bar'],
                        'exp' => time() + 1
                    ]
                )
            ]
        ];
    }

    /**
     * Retrieves ID Token Value to test validate ID Token in happy path
     *
     * @return array
     */
    public function getIdTokenDataInHappyPath()
    {
        $issuer         = 'https://issuer.com';
        $clientId       = 'clientId';
        $rsaKeyData     = 'RSA Key Data';

        return [
            [
                $issuer,
                $clientId,
                $rsaKeyData,
                json_encode([
                    'iss' => $issuer,
                    'aud' => $clientId,
                    'exp' => (time() + 10)
                ])
            ],
            [
                $issuer,
                $clientId,
                $rsaKeyData,
                json_encode([
                    'iss' => $issuer,
                    'azp' => $clientId,
                    'aud' => [$clientId, 'clientId2'],
                    'exp' => (time() + 10)
                ])
            ]
        ];
    }

    /**
     * Retrieves a mocked instance of JWSHelper
     *
     * @param bool $isValidSignature
     * @param bool $hasUnExpectedException
     * @param string $idToken
     *
     * @return JWSHelper
     *
     * @throws Exception
     */
    private function getJWSHelper(
        bool $isValidSignature = true,
        bool $hasUnExpectedException = false,
        string $idToken = ''
    ) {
        $jwsHelper = $this->getMockBuilder('Id4me\RP\Helper\JWSHelper')
                          ->disableOriginalConstructor()
                          ->setMethods(['verifySignature', 'getJWSPayload'])
                          ->getMock();

        if ($hasUnExpectedException) {
            $unexpectedException = new \Exception('Invalid RSA Key provided');
            $jwsHelper->expects($this->any())
                      ->method('verifySignature')
                      ->willThrowException($unexpectedException);
        } else {
            $jwsHelper->expects($this->any())
                      ->method('verifySignature')
                      ->will(
                          $this->returnValue($isValidSignature)
                      );

            $jwsHelper->expects($this->any())
                      ->method('getJWSPayload')
                      ->will(
                          $this->returnValue($idToken)
                      );
        }

        return $jwsHelper;
    }
}
