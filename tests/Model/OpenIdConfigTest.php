<?php

use \Id4me\RP\Model\OpenIdConfig;

class OpenIdConfigTest extends \PHPUnit\Framework\TestCase
{
    public function testInit()
    {
        $config = [
            "issuer" => "http://local.host"
        ];

        $c = new OpenIdConfig($config);
        $this->assertEquals($c->getIssuer(), $config["issuer"]);
    }

    public function testInitComplex()
    {
        $config = [
            "issuer" => "http://local.host",
            "token_endpoint" => "http://local.host/token",
            "not_existing_key" => "it_should_not_throw_error"
        ];

        $c = new OpenIdConfig($config);
        $this->assertEquals($c->getTokenEndpoint(), $config["token_endpoint"]);
    }
}
