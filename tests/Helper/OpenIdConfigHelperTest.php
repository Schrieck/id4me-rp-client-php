<?php

class OpenIdConfigHelperTest extends \PHPUnit\Framework\TestCase
{
    public function testCreateFromJson()
    {
        $input = '{"issuer": "http://local.host"}';

        $openIdConfig = new \Id4me\RP\Model\OpenIdConfig(["issuer" => "http://local.host"]);
        $helper = new \Id4me\RP\Helper\OpenIdConfigHelper();
        $openIdConfigFromHelper = $helper->createFromJson($input);

        $this->assertEquals($openIdConfig->getIssuer(), $openIdConfigFromHelper->getIssuer());
    }

    public function testCreateFromArray()
    {
        $input = ["issuer" => "http://local.host"];

        $openIdConfig = new \Id4me\RP\Model\OpenIdConfig($input);

        $helper = new \Id4me\RP\Helper\OpenIdConfigHelper();
        $openIdConfigFromHelper = $helper->createFromArray($input);

        $this->assertEquals($openIdConfig->getIssuer(), $openIdConfigFromHelper->getIssuer());
    }
}
