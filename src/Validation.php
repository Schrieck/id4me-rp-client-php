<?php

namespace Id4me\RP;

use Id4me\RP\Exception\InvalidAuthorityIssuerException;
use Id4me\RP\Exception\InvalidIDTokenException;
use Id4me\RP\Helper\JWSHelper;

/**
 * This class is responsible of providing utility function in order to validate authority data
 *
 * Following validation use cases are covered here:
 *
 * - ID Token:
 *  solely following requirements of ID4Me specifications 4.5.3. ID Token Validation: 1. to 5. and 9.
 */
class Validation
{
    const INVALID_ID_TOKEN_ISSUER_EXCEPTION = 'iss and issuer values are not equals';
    const INVALID_ID_TOKEN_AUDIENCE_EXCEPTION = 'Authority audience does not contains clientId';
    const INVALID_ID_TOKEN_EXPIRATION_TIME_EXCEPTION = 'Invalid ID Token expiration time';

    /**
     * @var JWSHelper
     */
    private $jwsHelper;

    /**
     * Validation constructor.
     */
    public function __construct()
    {
        $this->jwsHelper = new JWSHelper();
    }

    /**
     * Validates ID Token value retrieved by authority
     *
     * @param string $issuer
     * @param string $clientId
     * @param string $idToken
     * @param string $rsaKeyData
     *
     * @throws InvalidAuthorityIssuerException if iss and issuer values are not equal
     * @throws InvalidIDTokenException if provided id token value does not pass any of 13 validation steps
     *         described in ID Token Validation specifications of ID4Me Documentation
     */
    public function validateIDToken(string $issuer, string $clientId, string $idToken, string $rsaKeyData)
    {
        $this->validateIdTokenSignature($idToken, $rsaKeyData);

        $decryptedIDToken = $this->getDecryptedIdToken($idToken);
        if (! empty($decryptedIDToken)) {
            $this->validateISS($this->fetchTokenPropertyValue($decryptedIDToken, 'iss'), $issuer);
            $this->validateAudience($decryptedIDToken, $clientId);
            $this->validateExpirationTime($this->fetchTokenPropertyValue($decryptedIDToken, 'exp'));
        }
    }

    /**
     * Validates given authority issuer data
     *
     * Note this this the second validation step described in ID4Me specifications of ID Token Validation
     *
     * @param string  $originIssuer
     * @param string  $deliveredIssuer
     * @param boolean $exactMatch
     *
     * @throws InvalidAuthorityIssuerException if provided iss and issuer values are not equal
     */
    public function validateISS(string $originIssuer, string $deliveredIssuer, $exactMatch = true)
    {
        if ($exactMatch && ($originIssuer !== $deliveredIssuer)) {
            throw new InvalidAuthorityIssuerException(self::INVALID_ID_TOKEN_ISSUER_EXCEPTION);
        }

        if (!$exactMatch && !preg_match('/https?:\/\/' . preg_quote($originIssuer, '/') . '\/?/', $deliveredIssuer)) {
            throw new InvalidAuthorityIssuerException(self::INVALID_ID_TOKEN_ISSUER_EXCEPTION);
        }
    }

    /**
     * Validates given ID Token signature using given value and rsa key data
     *
     * Note if token signature passes validation an array list of token data to be validated will be retrieved
     *
     * @param string $idToken
     * @param string $rsaKeyData
     *
     * @throws InvalidIDTokenException
     */
    public function validateIdTokenSignature(string $idToken, string $rsaKeyData)
    {
        $isValidTokenSignature = null;

        try {
            $isValidTokenSignature = $this->jwsHelper->verifySignature($idToken, $rsaKeyData);
        } catch (\Exception $e) {
            throw new InvalidIDTokenException(
                sprintf('Invalid ID Token Signature provided - got error: %s', $e->getMessage())
            );
        }

        if (! $isValidTokenSignature) {
            throw new InvalidIDTokenException(
                sprintf('Invalid ID Token Signature: %s', $idToken)
            );
        }
    }

    /**
     * Validates audience data in given decrypted token value
     *
     * Validation will be done in accordance to specifications 3., 4. and 5. defined in 4.5.3. ID Token Validation
     *
     * @param array $decryptedIDToken
     * @param string $clientId
     *
     * @throws InvalidIDTokenException
     */
    public function validateAudience(array $decryptedIDToken, string $clientId)
    {
        $isValidAudience = false;

        $audience = $this->fetchTokenPropertyValue($decryptedIDToken, 'aud');

        if (! empty($audience)) {
            if (is_array($audience)) {
                $isValidAudience = $this->isValidMultipleAudience($decryptedIDToken, $audience, $clientId);
            } elseif ($audience == $clientId) {
                $isValidAudience = true;
            } elseif ($audience != $clientId) {
                // check if `azd` matches client-id
                $authorizedParty = $this->fetchTokenPropertyValue($decryptedIDToken, 'azp');
                if ($authorizedParty == $clientId) {
                    $isValidAudience = true;
                }
            }
        }

        if (! $isValidAudience) {
            throw new InvalidIDTokenException(self::INVALID_ID_TOKEN_AUDIENCE_EXCEPTION);
        }
    }

    /**
     * Checks if given audience data in constellation of multiple audience (clientId list) are valid
     *
     * In this case verification is done in accordance to specifications 4. and 5. defined in 4.5.3. ID Token Validation
     *
     * @param array $decryptedIDToken
     * @param array $audience
     * @param string $clientId
     *
     * @return bool
     */
    public function isValidMultipleAudience(array $decryptedIDToken, array $audience, string $clientId)
    {
        if ((count($audience) <= 1) && (current($audience) == $clientId)) {
            return true;
        }

        $authorizedParty = $this->fetchTokenPropertyValue($decryptedIDToken, 'azp');

        if ($authorizedParty && ($authorizedParty != $clientId)) {
            return false;
        }

        return in_array($clientId, $audience);
    }

    /**
     * Validates ID Token expiration time
     *
     * @param string $expirationTime
     *
     * @throws InvalidIDTokenException
     */
    public function validateExpirationTime(string $expirationTime)
    {
        $isValidExpirationTime = false;

        if (! empty($expirationTime)) {
            $isValidExpirationTime = (time() < intval($expirationTime));
        };

        if (! $isValidExpirationTime) {
            throw new InvalidIDTokenException(self::INVALID_ID_TOKEN_EXPIRATION_TIME_EXCEPTION);
        }
    }

    /**
     * Sets value of JW Helper to another value
     *
     * @param JWSHelper $jwsHelper
     */
    public function setJWSHelper(JWSHelper $jwsHelper)
    {
        $this->jwsHelper = $jwsHelper;
    }

    /**
     * Retrieves an array list containing all token data to be validated
     *
     * @param string $idToken
     *
     * @return array
     *
     * @throws InvalidIDTokenException
     */
    private function getDecryptedIdToken(string $idToken)
    {
        try {
            return json_decode(
                $this->jwsHelper->getJWSPayload($idToken),
                true
            );
        } catch (\Exception $exception) {
            throw new InvalidIDTokenException(
                sprintf('Could not decrypt given ID Token: %s', $exception->getMessage())
            );
        }
    }

    /**
     * Extracts token property value out of given token data list if found
     *
     * @param array $tokenDataList
     * @param string $property
     *
     * @return mixed
     */
    private function fetchTokenPropertyValue(array $tokenDataList, string $property)
    {
        return (array_key_exists($property, $tokenDataList)) ? $tokenDataList[$property] : null;
    }
}
