<?php

namespace Id4me\RP\Helper;

use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Core\Converter\StandardConverter;
use Jose\Component\Signature\JWSVerifier;
use Jose\Component\Signature\Serializer\CompactSerializer;
use Jose\Component\Signature\Serializer\JWSSerializerManager;

/**
 * This class is responsible of handling RSA encryption and decryption functions
 * used for example for validation of authority data fetched by Relying Party
 */
class JWSHelper
{
    /**
     * Retrieves a JWS out of given jws json string
     *
     * @param string $jws
     *
     * @return \Jose\Component\Signature\JWS
     *
     * @throws \Exception
     */
    public function createJWSFromString(string $jws)
    {
        $jsonConverter = new StandardConverter();

        $serializerManager = JWSSerializerManager::create([
            new CompactSerializer($jsonConverter),
        ]);

        return $serializerManager->unserialize($jws);
    }

    /**
     * Verifies given jws token string with given jws data set string ()
     *
     * @param string $jwsTokenString
     * @param string $jwsSetJson
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function verifySignature(string $jwsTokenString, $jwsSetJson)
    {
        $jws = $this->createJWSFromString($jwsTokenString);
        $jwkSetFromJWKSFile = \Jose\Component\Core\JWKSet::createFromJson($jwsSetJson);
        $jwsVerifier = $this->getJWSVerifier();

        return $jwsVerifier->verifyWithKeySet($jws, $jwkSetFromJWKSFile, 0);
    }

    /**
     * Retrieves JWS payload of decrypted value of given jws token string
     *
     * @param string $jwsTokenString
     *
     * @return string
     *
     * @throws \Exception
     */
    public function getJWSPayload(string $jwsTokenString)
    {
        return $this->createJWSFromString($jwsTokenString)->getPayload();
    }

    /**
     * Retrieves an instance of JW verifier
     *
     * @return JWSVerifier
     */
    protected function getJWSVerifier()
    {
        $algorithmManager = AlgorithmManager::create([
            new \Jose\Component\Signature\Algorithm\RS256(),
        ]);

        return new JWSVerifier(
            $algorithmManager
        );
    }
}
